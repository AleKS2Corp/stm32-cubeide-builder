# STM32 CubeIDE Builder

[![pipeline status](https://gitlab.com/AleKS2Corp/stm32-cubeide-builder/badges/main/pipeline.svg)](https://gitlab.com/AleKS2Corp/stm32-cubeide-builder/-/commits/main)
[![Latest Release](https://gitlab.com/AleKS2Corp/stm32-cubeide-builder/-/badges/release.svg)](https://gitlab.com/AleKS2Corp/stm32-cubeide-builder/-/releases)