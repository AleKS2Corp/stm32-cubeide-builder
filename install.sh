#!/bin/bash

apt-get update
apt-get install -y wget zip git

mkdir -p /_tmp
cd /_tmp
wget --no-check-certificate -qO CubeIDE https://www.st.com/content/ccc/resource/technical/software/sw_development_suite/group0/13/d4/6b/b0/d2/fd/47/6d/stm32cubeide_deb/files/st-stm32cubeide_1.9.0_12015_20220302_0855_amd64.deb_bundle.sh.zip/jcr:content/translations/en.st-stm32cubeide_1.9.0_12015_20220302_0855_amd64.deb_bundle.sh.zip
unzip -p CubeIDE > stm32cubeide-installer.sh
rm CubeIDE
chmod +x stm32cubeide-installer.sh
./stm32cubeide-installer.sh
rm stm32cubeide-installer.sh
cd ..
rm -rf _tmp

apt-get -y -f install
apt-get clean
